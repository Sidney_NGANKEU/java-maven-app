#!/bin/bash

# install docker

sudo yum update -y && yum install docker -y
sudo systemctl start docker
sudo usermod -aG docker ec2-user

# install docker-compose

sudo curl -SL "https://github.com/docker/compose/releases/download/v2.20.3/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
