### My variables #####

variable env_prefix {
    default = "dev"
}
variable avail_zone {
    default = "eu-central-1a"
}
variable vpc_cidr_bloc {
    default = "10.0.0.0/16"
}
variable subnet_cidr_bloc {
    default = "10.0.0.0/24"
}
variable my_ip {
    default = "82.66.221.66/32"
}


variable jenkins_ip {
    default = "164.92.161.235/32"
}

variable instance_type {
    default = "t2.micro"
}

variable region {
    default = "eu-central-1"
}