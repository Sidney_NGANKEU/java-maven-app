FROM amazoncorretto:8-alpine3.17-jre

EXPOSE 8080

COPY ./target/java-maven-app-*.jar /usr/app/
WORKDIR /usr/app
ENV POSTGRES_PASSWORD=my-pwd
CMD java jar java-maven-app-*.jar
